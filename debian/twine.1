.TH twine 1 "Sep 13, 2024" "Client to upload on pypi"
.SH NAME
twine
\- Client to do uploads on pypi.
.SH DESCRIPTION
This command is commonly used to upload python modules on pypi.org or compatible repositories.
.SH NOTES ON PYPI.ORG
This command can be used on multiple repositories. However the default is to use pypi.org, and since it is probably the most common use case, this section is dedicated to its otherwise undocumented quirks.
.TP
.B Upload via token
Username and password uploads are no longer supported on pypi.org.
.br
To upload authenticating with a token, the command looks like this:
        twine upload --username __token__ --password `cat .token` dist/
.TP
.B Scope limited token
Tokens can be obtained from the website of pypi.org.
.br
To create a token that is limited to a project that doesn't yet exist, a global token must be created, used to do the first upload of the project, then deleted from the website. At that point it will be possible to generate a token that has the scope of that project.
.TP
.B Signing
Signing is not allowed on pypi.org.
.br
Uploads that are signed are greeted by a warning email. The signature files are dropped. In the future they might cause the upload to fail.

.SH SYNOPSIS
twine [-h] [--version] [--no-color] [<command>] [args]
.SS
.SS Options:
.TP
.B -h, --help
Show the help message and exit.
.TP
.B -v, --version
Show the version and exit.
.TP
.B --no-color
Disable coloured output.
.SS
.SS Commands:
.TP
.B check
.RS 4
usage: twine check [-h] [--strict] dist [dist ...]

.B positional arguments:
  dist        The distribution files to check, usually dist/*

.B options:
  -h, --help  show this help message and exit
  --strict    Fail on warnings
.RE

.TP
.B register
.RS 4
usage: twine register [-h] [-r REPOSITORY] [--repository-url REPOSITORY_URL] [--attestations] [-s] [--sign-with SIGN_WITH] [-i IDENTITY] [-u USERNAME] [-p PASSWORD] [--non-interactive] [-c COMMENT] [--config-file CONFIG_FILE] [--skip-existing] [--cert path] [--client-cert path] [--verbose] [--disable-progress-bar] package

register operation is not required with PyPI.org

.B positional arguments:
  package               File from which we read the package metadata.

.B options:
  -h, --help            show this help message and exit
  -r REPOSITORY, --repository REPOSITORY
                        The repository (package index) to upload the package to. Should be a section in the config file (default: pypi). (Can also be set via TWINE_REPOSITORY environment variable.)
  --repository-url REPOSITORY_URL
                        The repository (package index) URL to upload the package to. This overrides --repository. (Can also be set via TWINE_REPOSITORY_URL environment variable.)
  --attestations        Upload each file's associated attestations.
  -s, --sign            Sign files to upload using GPG.
  --sign-with SIGN_WITH
                        GPG program used to sign uploads (default: gpg).
  -i IDENTITY, --identity IDENTITY
                        GPG identity used to sign files.
  -u USERNAME, --username USERNAME
                        The username to authenticate to the repository (package index) as. (Can also be set via TWINE_USERNAME environment variable.)
  -p PASSWORD, --password PASSWORD
                        The password to authenticate to the repository (package index) with. (Can also be set via TWINE_PASSWORD environment variable.)
  --non-interactive     Do not interactively prompt for username/password if the required credentials are missing. (Can also be set via TWINE_NON_INTERACTIVE environment variable.)
  -c COMMENT, --comment COMMENT
                        The comment to include with the distribution file.
  --config-file CONFIG_FILE
                        The .pypirc config file to use.
  --skip-existing       Continue uploading files if one already exists. (Only valid when uploading to PyPI. Other implementations may not support this.)
  --cert path           Path to alternate CA bundle (can also be set via TWINE_CERT environment variable).
  --client-cert path    Path to SSL client certificate, a single file containing the private key and the certificate in PEM format.
  --verbose             Show verbose output.
  --disable-progress-bar
                        Disable the progress bar.

.RE

.TP
.B upload

usage: twine upload [-h] [-r REPOSITORY] [--repository-url REPOSITORY_URL] [--attestations] [-s] [--sign-with SIGN_WITH] [-i IDENTITY] [-u USERNAME] [-p PASSWORD] [--non-interactive] [-c COMMENT] [--config-file CONFIG_FILE] [--skip-existing] [--cert path] [--client-cert path] [--verbose] [--disable-progress-bar] dist [dist ...]

.RS 4
.B positional arguments:
  dist                  The distribution files to upload to the repository (package index). Usually dist/* . May additionally contain a .asc file to include an existing signature with the file upload.

.B options:
  -h, --help            show this help message and exit
  -r REPOSITORY, --repository REPOSITORY
                        The repository (package index) to upload the package to. Should be a section in the config file (default: pypi). (Can also be set via TWINE_REPOSITORY environment variable.)
  --repository-url REPOSITORY_URL
                        The repository (package index) URL to upload the package to. This overrides --repository. (Can also be set via TWINE_REPOSITORY_URL environment variable.)
  --attestations        Upload each file's associated attestations.
  -s, --sign            Sign files to upload using GPG.
  --sign-with SIGN_WITH
                        GPG program used to sign uploads (default: gpg).
  -i IDENTITY, --identity IDENTITY
                        GPG identity used to sign files.
  -u USERNAME, --username USERNAME
                        The username to authenticate to the repository (package index) as. (Can also be set via TWINE_USERNAME environment variable.)
  -p PASSWORD, --password PASSWORD
                        The password to authenticate to the repository (package index) with. (Can also be set via TWINE_PASSWORD environment variable.)
  --non-interactive     Do not interactively prompt for username/password if the required credentials are missing. (Can also be set via TWINE_NON_INTERACTIVE environment variable.)
  -c COMMENT, --comment COMMENT
                        The comment to include with the distribution file.
  --config-file CONFIG_FILE
                        The .pypirc config file to use.
  --skip-existing       Continue uploading files if one already exists. (Only valid when uploading to PyPI. Other implementations may not support this.)
  --cert path           Path to alternate CA bundle (can also be set via TWINE_CERT environment variable).
  --client-cert path    Path to SSL client certificate, a single file containing the private key and the certificate in PEM format.
  --verbose             Show verbose output.
  --disable-progress-bar
                        Disable the progress bar.
.SH ENVIRONMENT
The following environment variables can be used instead of command line options.
.TP
.B TWINE_USERNAME
.TP
.B TWINE_PASSWORD
.TP
.B TWINE_REPOSITORY_URL
.TP
.B TWINE_REPOSITORY
.TP
.B TWINE_CERT
.TP
.B TWINE_NON_INTERACTIVE
.SH "SEE ALSO"
.BR twine(3)
.SH AUTHOR
This manual page was written for Debian by Salvo 'LtWorf' Tomaselli <ltworf@debian.org>, but may freely be used by others.
